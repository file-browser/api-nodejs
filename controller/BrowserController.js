import { EnvironmentConfig } from '../config/EnvironmentConfig'

const fs = require('fs');

function readDirectory(path) {
    return new Promise((resolve, reject) => {
        fs.readdir(path, { withFileTypes: true }, (err, files) => {
            resolve(files);
            reject(err);
        });
    })
};

module.exports.getFolderContent = (req, res) => {
    var filesList = [];
    var path = req.params.path;

    var actualPath = path ? EnvironmentConfig.pathConfig + '/' + path.split('*').join('/') : EnvironmentConfig.pathConfig + '/' + '';

    return readDirectory(actualPath)
        .then((res) => {
            res.forEach(file => {
                var object = {
                    fileName: '',
                    isDirectory: ''
                };
                object.fileName = file.name;
                object.isDirectory = file.isDirectory();

                filesList.push(object);
            });
        })
        .then(() => {
            res.status(200).send(filesList);
        })
        .catch(err => {
            console.log(err);
            res.status(500).send(err);
        })
}

module.exports.downloadFile = (req, res) => {
    var path = req.params.path;

    var actualPath = path ? EnvironmentConfig.pathConfig + '/' + path.split('*').join('/') : EnvironmentConfig.pathConfig + '/' + '';
    
    var filePath = actualPath + '/' + req.params.fileName

    res.download(filePath, req.params.fileName);
}

module.exports.uploadFile = (req, res) => {
    res.status(200).send('ok')
}

module.exports.deleteFile = (req, res) => {
    var path = req.body.path 

    var actualPath = path ? EnvironmentConfig.pathConfig + path.split('*').join('/') : EnvironmentConfig.pathConfig;

    try {
        fs.unlinkSync(actualPath + '/' + req.body.file)
        res.status(200).send('ok')
    } catch (error) {
        res.status(500).error(error)
    }
    
}