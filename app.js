import express from 'express';
import bodyParser from 'body-parser';
import { EnvironmentConfig } from './config/EnvironmentConfig'
import { getFolderContent, uploadFile, downloadFile, deleteFile } from './controller/BrowserController'
var multer  = require('multer')

var storage = multer.diskStorage({
  destination: function (req, file, cb) {
    cb(null, req.params.path ? EnvironmentConfig.pathConfig + req.params.path.split('*').join('/') : EnvironmentConfig.pathConfig)
  },
  filename: function (req, file, cb) {
    cb(null, file.originalname)
  }
})

var upload = multer({ storage: storage })


let post = bodyParser.urlencoded({ extended: true });
var cors = require('cors')
var app = express();
app.use(cors())
app.use(express.json());

var corsOptions = {
    origin: '*',
    optionsSuccessStatus: 200 // some legacy browsers (IE11, various SmartTVs) choke on 204
  }

app.listen(3001, () => {
 console.log("Server running on port 3001");
});

app.get('/', (req, res) => {res.status(200).send("Bonjour")});

app.get('/browser/:path?', (req, res) => { getFolderContent(req, res) })

app.get('/download/:fileName/:path?', (req, res) => { downloadFile(req, res) })

app.post('/upload/:path?', upload.single('file') ,(req, res) => { uploadFile(req, res) })

app.delete('/delete', (req, res) => { deleteFile(req,res) })