# File Browser Api

## Introduction
This API handles 4 routes in order to navigate into folders and manipulate files.
You must choose the main folder to navigate through. To do so edit the **EnvironmentConfig.js** file located in the **config** folder.
Fill in the **pathConfig** property with the desired path (without the last '/').
**Like:**

    /User/You/Documents

## Routes

**/browser/:path?**
To get the content at the given path.

**/download/:filename/:path?**
To download a file.

**/upload/:path?**
To upload a file at the given path.

**/delete**
To delete a file.

## Get Started
### Prerequisites
**nodemon**

    npm install -g nodemon

**nodejs v10.10+**

### Launch
First execute in the root directory:

    npm install
Then to launch the API use:

    npm start
By default the server uses the *3001* port, you can edit the line 30 in the file **app.js**.